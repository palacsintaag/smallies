/**
 * Created by kenguru on 2019.02.24..
 */

$(document).ready(function(){
    random_side_color();
});


function random_side_color(){
    $(".side-color").each(function () {
        var x = Math.floor(Math.random() * 256);
        var y = Math.floor(Math.random() * 256);
        var z = Math.floor(Math.random() * 256);
        var bgColor = "rgb(" + x + "," + y + "," + z + ")";
        $(this).css("background-color", bgColor);
    });
}