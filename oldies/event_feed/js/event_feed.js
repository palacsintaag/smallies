/**
 * Created by kenguru on 2017.01.10..
 */

$(document).ready(function(){
     show_n_feed_hide_others();
});

function show_n_feed_hide_others(){
    $('ul#feed li:gt(2)').hide();

    $('a#show_more').click(function () {
        $('ul#feed li:gt(2)').toggle();
    });

}